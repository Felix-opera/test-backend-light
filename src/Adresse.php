<?php

declare(strict_types=1);

namespace App;

final class Adresse
{
    private string $nomRue;
    private int $numeroRue;
    private int $codePostal;

    public function __construct(
        string $nomRue,
        int $numeroRue,
        int $codePostal
    ){
        $this->nomRue = $nomRue;
        $this->numeroRue = $numeroRue;
        $this->codePostal = $codePostal;
    }

    public function nomRue(): string
    {
        return $this->nomRue;
    }

    public function numeroRue(): int
    {
        return $this->numeroRue;
    }

    public function codePostal(): int
    {
        return $this->codePostal;
    }
    
}