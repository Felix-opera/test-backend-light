<?php

declare(strict_types=1);

namespace App;

/**
 * Compte les caractères de la chaîne 
 * passée en paramètre, sans prendre en compte la casse.
 * 
 * @return array tabeau avec en clef le caractère et en valeur le nombre d'occurence
 */
function compterCaractères(string $str): array
{
    return [];
}

/**
 * Valide que la chaîne d'entrée est une IP valide
 */
function validerAdresseIP(string $ip): bool
{
    return false;
}

/**
 * Formate en csv les users passés en paramètre
 */
function formaterUserCSV(User ...$user): string
{
    return '';
}
