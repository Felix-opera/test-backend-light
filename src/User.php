<?php

declare(strict_types=1);

namespace App;

final class User
{
    private string $nom;
    private Adresse $adresse;

    public function __construct(string $nom, Adresse $adresse)
    {
        $this->nom = $nom;
        $this->adresse = $adresse;
    }

    public function nom(): string
    {
        return $this->nom;
    }

    public function adresse(): Adresse
    {
        return $this->adresse;
    }
} 
