Des tests unitaires sont écrits avec phpunit. Pour les valider lancer simplement la commande :
`vendor/bin/phpunit tests/`

ou bien les fonctions une par une :
`vendor/bin/phpunit tests/ --filter=test_compter_caractères`
`vendor/bin/phpunit tests/ --filter=test_ip`
`vendor/bin/phpunit tests/ --filter=test_users`

# Exercice 1
Ecrire une fonction qui prend en paramètre une chaine de caractère, et qui retourne un tableau correspondant du nombre d'occurence de chaque caractère.

# Exercice 2
Fonction qui valide le bon format d'une adresse ip.

# Exercice 3
Formater un tableau de User. La classe User est présente dans src/User.php
Le format attendu est à trouver dans le test unitaire associé.