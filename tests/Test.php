<?php

declare(strict_types=1);

namespace App\Test;

use App\Adresse;
use App\User;
use PHPUnit\Framework\TestCase;

use function App\compterCaractères;
use function App\formaterUserCSV;
use function App\validerAdresseIP;

class Test extends TestCase
{
    public function test_compter_caractères(): void
    {        
        $this->assertEquals([
            ' ' => 1,
            'l' => 1,
            'r' => 1,
            'o' => 1,
            'e' => 1,
            'm' => 2,
            'i' => 1,
            'p' => 1,
            's' => 1,
            'u' => 1,
        ], compterCaractères('Lorem Ipsum'));
        
        $this->assertEquals(                
            json_decode('{"c":5,"o":12,"n":6,"t":11,"r":11,"a":11,"y":3," ":29,"p":5,"u":3,"l":9,"b":2,"e":10,"i":12,"f":3,",":2,"m":6,"s":8,"d":2,"x":1,".":2,"h":1,"4":1,"5":1,"k":1,"g":1,"v":1,"2":1,"0":3}', true),
            compterCaractères('Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.')
        );

        $this->assertEquals(
            'Fascinated by the possibilities offered by IT, he takes pleasure in discovering the issues and understanding the needs in order to offer simple and effective implementations.',
            json_decode('{"f":7,"a":8,"s":14,"c":3,"i":16,"n":12,"t":12,"e":24,"d":9," ":25,"b":3,"y":2,"h":4,"p":4,"o":7,"l":4,"r":7,",":1,"k":1,"u":3,"v":2,"g":2,"m":3,".":1}', true)
        );
    }

    public function test_ip(): void
    {
        $this->assertTrue(validerAdresseIP('125.12.2.21'));
        $this->assertTrue(validerAdresseIP('9.128.21.21'));
        $this->assertTrue(validerAdresseIP('127.0.0.1'));
        $this->assertTrue(validerAdresseIP('255.255.255.255'));
        $this->assertTrue(validerAdresseIP('0.0.0.0'));

        $this->assertFalse(validerAdresseIP('lol'));
        $this->assertFalse(validerAdresseIP(''));
        $this->assertFalse(validerAdresseIP('987.654.32.1'));
        $this->assertFalse(validerAdresseIP('0.1.2.1111'));
        $this->assertFalse(validerAdresseIP('....'));
        $this->assertFalse(validerAdresseIP('8.98.257.1'));
    }

    public function test_users(): void
    {
        $user1 = new User(
            'Miguel',
            new Adresse("rue de la street", 12, 69003)
        );
        $user2 = new User(
            'Bernadette Chirac',
            new Adresse("rue de la vendée", 37, 49280)
        );
        $user3 = new User(
            'Bernard Campan',
            new Adresse("Boulevard des capucine", 254, 13650)
        );

        $this->assertSame(
            "Miguel;12 rue de la street;69003\rBernadette Chirac;37 rue de la vendée;49280\rBernard Campan;254 Boulevard des capucine;13654",
            formaterUserCSV($user1, $user2, $user3)
        );
    }
}
